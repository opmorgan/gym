gym_tidy <- function(data) {
  data <- data %>% 
    gather(key = exercise_measure,
           value = value,
           starts_with('deadlift'),
           starts_with('press'),
           starts_with('squat'),
           starts_with('bench'),
           starts_with('clean'),
           starts_with('chinup')
    ) %>%
    separate(exercise_measure,
             into = c('exercise', 'measure')
    ) %>%
    select('exercise', 'measure', 'value', everything()) %>% 
    group_by(measure) %>% 
    mutate(grouped_id = row_number()) %>% 
    spread(measure, value) %>% 
    select(-grouped_id)
  
  ## for now, just look at weightlifting variables
  data <- data %>% select('day', 'exercise', 'sets', 'reps', 'lb', 'date')
  
  ## fix formatting
  data$date = mdy(data$date)
  data$sets <-  as.numeric(data$sets)
  data$reps <-  as.numeric(data$reps)
  data$lb <-  as.numeric(data$lb)
  
  return(data)
}